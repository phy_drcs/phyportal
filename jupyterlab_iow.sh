source ~/.bashrc
source ~/.bash_profile
module load miniconda3
ncores=1
export OMP_NUM_THREADS=$ncores
export OPENBLAS_NUM_THREADS=$ncores
export MKL_NUM_THREADS=$ncores
export VECLIB_MAXIMUM_THREADS=$ncores
export NUMEXPR_NUM_THREADS=$ncores
jupyter lab
