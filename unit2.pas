unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Spin, Buttons;

type

  { TForm2 }

  TForm2 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Shape1: TShape;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

{ TForm2 }

procedure TForm2.ComboBox1Select(Sender: TObject);
begin
  // in a queue called "test", jobs can run no longer than 1 hour by default
  if ComboBox1.Items.Count>0 then
    if pos('test',ComboBox1.Items[ComboBox1.ItemIndex])>0 then
      SpinEdit2.Value:=1;
end;

procedure TForm2.BitBtn1Click(Sender: TObject);
begin
  Form2.Tag:=1;
  Form2.Close;
end;

procedure TForm2.BitBtn2Click(Sender: TObject);
begin
  Form2.Close;
end;

end.

