unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  MaskEdit, Buttons;

type

  { TForm3 }

  TForm3 = class(TForm)
    BitBtn1: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Shape1: TShape;
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Edit5Change(Sender: TObject);
    procedure Edit6Change(Sender: TObject);
  private

  public

  end;

var
  Form3: TForm3;

implementation

{$R *.lfm}

{ TForm3 }

procedure TForm3.BitBtn1Click(Sender: TObject);
begin
  Form3.Close;
end;

procedure TForm3.Edit1Change(Sender: TObject);
begin
  if (length(Edit1.Text)>0) and (length(Edit2.Text)>0) then
    Button1.visible:=true
  else
    Button1.visible:=false;
end;

procedure TForm3.Edit2Change(Sender: TObject);
begin
  if (length(Edit1.Text)>0) and (length(Edit2.Text)>0) then
    Button1.visible:=true
  else
    Button1.visible:=false;
end;

procedure TForm3.Edit3Change(Sender: TObject);
begin
  if (length(Edit3.Text)>0) and (length(Edit4.Text)>0) then
    Button2.visible:=true
  else
    Button2.visible:=false;
end;

procedure TForm3.Edit4Change(Sender: TObject);
begin
  if (length(Edit3.Text)>0) and (length(Edit4.Text)>0) then
    Button2.visible:=true
  else
    Button2.visible:=false;
end;

procedure TForm3.Edit5Change(Sender: TObject);
begin
  if (length(Edit5.Text)>0) and (length(Edit6.Text)>0) then
    Button3.visible:=true
  else
    Button3.visible:=false;
end;

procedure TForm3.Edit6Change(Sender: TObject);
begin
  if (length(Edit5.Text)>0) and (length(Edit6.Text)>0) then
    Button3.visible:=true
  else
    Button3.visible:=false;
end;

end.

