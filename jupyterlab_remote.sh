#!/bin/bash
#
#SBATCH --job-name=jupyter
#SBATCH --output=/data/%u/.jupyter_out.txt
#SBATCH --nodes 1
#SBATCH --ntasks=1
#SBATCH -t 8:00:00
#SBATCH -p compute

source ~/.bashrc
PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

export MODULEPATH=$MODULEPATH:/data/iow/iow-modules/DATAFORMATS
export MODULEPATH=$MODULEPATH:/data/iow/iow-modules/VISUALISATION
export MODULEPATH=$MODULEPATH:/data/iow/iow-modules/MPI
export MODULEPATH=$MODULEPATH:/data/iow/iow-modules/NUMERICS
module load miniconda3

export OMP_NUM_THREADS=40
export KMP_AFFINITY=compact,1,0,granularity=fine
export MKLDNN_VERBOSE=1
export MYJUPYTERPORT=40374
export XDG_RUNTIME_DIR="/data/$USER"
hostname > /data/hr275/.jupyterlab_hostname.txt
jupyter lab --port $MYJUPYTERPORT --ip 0.0.0.0 --no-browser --notebook-dir="/data/$USER"
