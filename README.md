# phyportal

Desktop application to see IOW server load and connect to IOW and external servers via jupyterlab and xpra

## Building the code

* Get the Lazarus IDE, typically from your Linux distribution or from https://www.lazarus-ide.org/
* Run the Lazarus ide
* Open "phyportal.lpi"
* Click the green run button to build and run the program
* Later you can run directly without Lazarus

## Prerequisites to run the software

* Windows 10 or a common up-to-date Linux distribution
* Connection to IOW
* a file ~/.ssh/config which contains the following hosts: 
  * rdpserv 
  * haumea1 
  * blogin 
  * glogin
* the corresponding keys, which need to be named
  * ~/.ssh/id_rsa_iow
  * ~/.ssh/id_rsa_unirostock
  * ~/.ssh/id_rsa_hlrn
* at IOW /home/nis/username/.ssh/config, passwordless connections to the phy machines
* an ssh key manager which allows to add keys using ssh-add 